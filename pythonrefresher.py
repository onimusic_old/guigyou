age = 27


name = 'nelson'

new_name = 'Meu nome é {}'.format(name)

boleano = False

if age > 28 and boleano:
    print('VOcê é velho cara')
else:
    print('Você está chegando lá')
print(new_name)



year = 1830 # When you check your solution, don't change this number


if 2000 <= year <= 2100:
    print('Welcome to the 21st century!')
else:
    print('You are before or after the 21st century')



def test(name, age) -> str:
    """Example function with PEP 484 type annotations.

    Args:
        name (str): The first parameter. Onome.
        age (int): The second parameter.

    Returns:
        The return value. True for success, False otherwise.

    """
    return '{} {}'.format(name, str(age))



test('qs',123)


def doc_test(nelson: int, mandela: int) -> int:
    """ É isso ai

    :param nelson:
    :param mandela:
    :return:
    :rtype: int
    """
    return nelson+mandela


# trabalhando com arrays
array = []
for i in range(0, 10):
    # array[len(array):] = i+1
    array.append(i)

# removendo item da array
del(array[1])
print(range(10))
print(array)
print(array[:5])
print(array[2:5])
print(array[6:])
print(array[len(array):])
print(len(array))

