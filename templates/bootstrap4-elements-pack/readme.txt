Bootstrap-4 Elements Pack is elements of bootstrap-4.These are Testimonial,Pricing Table, Member, Tabs, Progress Bar, and Accordian. 
It is a fully responsive, feature rich and beautifully designed.

Your main code reside in Snippet directory. You have to upload every file from main directory to your server for properly work.

For info please refer documentation.

Thank you for purchasing Bootstrap-4 Elements Pack. If you have any questions that are beyond the scope of this help file and documentation, please feel free to contact us at webmaster@makewebbetter.com.

Flat Icon(https://www.flaticon.com/)
Icons made by 
https://www.flaticon.com/authors/pixel-perfect pixel-perfect,
https://www.flaticon.com/authors/good-ware Good Ware, 
https://www.flaticon.com/authors/smalllikeart smalllikeart, 
From
https://www.flaticon.com/ FlatIcon
