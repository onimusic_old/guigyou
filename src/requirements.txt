amqp==2.4.1
billiard==3.5.0.5
celery==4.2.1
certifi==2019.6.16
chardet==3.0.4
Django==2.1.5
django-appconf==1.0.3
django-bootstrap-datepicker-plus==3.0.5
django-bootstrap3==11.1.0
django-celery-beat==1.4.0
django-celery-email==2.0.2
django-celery-results==1.0.4
django-cleanup==4.0.0
django-crispy-forms==1.7.2
django-impersonate==1.4.1
django-post-office==3.2.1
django-timezone-field==3.0
gunicorn==19.9.0
idna==2.8
Jinja2==2.10
jsonfield==2.0.2
kombu==4.3.0
MarkupSafe==1.1.1
Pillow==5.4.1
psycopg2==2.7.7
psycopg2-binary==2.7.7
python-crontab==2.3.6
python-dateutil==2.8.0
pytz==2018.9
requests==2.21.0
six==1.12.0
urllib3==1.24.1
uuid==1.30
vine==1.2.0
