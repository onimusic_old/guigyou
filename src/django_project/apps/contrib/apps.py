from django.apps import AppConfig


class ArtistsConfig(AppConfig):
    name = 'django_project.apps.contrib'
    label = 'contrib'
