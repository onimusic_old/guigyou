from hashlib import md5
from django import template
from django_project.apps.core.models.base import get_tutorial_video_by_user

register = template.Library()



@register.simple_tag(name='user_avatar')
def user_avatar(user, size=35):
    if user:
        email = str(user.email.strip().lower()).encode('utf-8')
        email_hash = md5(email).hexdigest()
        url = "//www.gravatar.com/avatar/{0}?s={1}&d=identicon&r=PG"
        return url.format(email_hash, size)
    else:
        return None


@register.simple_tag(name='user_name')
def user_name(user):
    if user:
        if len(user.first_name) > 10:
            return user.first_name[:9]+'...'
        else:
            return user.first_name
    else:
        return None


@register.simple_tag(name='user_tutorial_video')
def user_tutorial_video(user):
    if user:
        return get_tutorial_video_by_user(user)
    else:
        return None
