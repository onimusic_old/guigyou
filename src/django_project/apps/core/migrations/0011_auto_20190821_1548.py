# Generated by Django 2.1.5 on 2019-08-21 18:48

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0010_remove_musician_events'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='artist',
            name='name',
        ),
        migrations.RemoveField(
            model_name='musicdirector',
            name='name',
        ),
        migrations.RemoveField(
            model_name='musician',
            name='name',
        ),
        migrations.RemoveField(
            model_name='producer',
            name='name',
        ),
    ]
