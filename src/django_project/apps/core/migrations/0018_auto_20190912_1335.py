# Generated by Django 2.1.5 on 2019-09-12 16:35

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0017_comment'),
    ]

    operations = [
        migrations.RenameField(
            model_name='comment',
            old_name='author_id',
            new_name='author',
        ),
        migrations.RenameField(
            model_name='comment',
            old_name='event_id',
            new_name='event',
        ),
    ]
