from django.contrib import admin
from .models.base import ModelClass


@admin.register(ModelClass)
class ArtistAdmin(admin.ModelAdmin):
    """Default Admin model for Artist
    """
    list_display = [
        'name',
        'type',
        'date',
        'status'
    ]
