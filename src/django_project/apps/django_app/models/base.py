"""Core Models to be used throughout the project
"""
from django.db import models

from django.utils.translation import ugettext_lazy as _

from django_project.apps.contrib.models.base_model import BaseModel

example_choices = (
    ('streams', 'streams'),
    ('users', 'users'),
    ('tracks', 'tracks'),
    ('sub_30_sec_streams', 'sub_30_sec_streams'),
    ('aggregatedstreams', 'aggregatedstreams'),
)


# Create your models here.
class ModelClass(BaseModel):
    name = models.CharField(max_length=10, default='')
    type = models.CharField(max_length=20, choices=example_choices)
    date = models.DateField()
    status = models.CharField(max_length=10, choices=(
        ('pending', 'pending'), ('error', 'error'), ('success', 'success')), default='pending')  # year or unknown

    class Meta:
        verbose_name = _('Example Model')
        verbose_name_plural = _('Example Models')

    def __str__(self):
        return str(self.type) + " - " + str(self.name)
