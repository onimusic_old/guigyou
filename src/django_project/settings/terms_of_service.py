from django.utils.html import format_html
from django.utils.translation import ugettext as _

def get_terms(user_type):
    if user_type == 'artist':
        return html_artist_terms + html_producer_terms
    if user_type == 'producer':
        return html_producer_terms
    if user_type == 'musician':
        return html_musician_terms
    if user_type == 'music_director':
        return html_music_director_terms

def get_artist_terms():
    return 'termos do artista'

def get_producer_terms():
    return 'termos do produtor'

def get_music_director_terms():
    return 'termos do diretor musical'

def get_musician_terms():
    return 'termos do musico'

html_artist_terms = format_html('<h4>{} {}</h4><p>{}</p>'.format(_('Terms of Service'), _('Artist'), get_artist_terms()))
html_producer_terms = format_html('<h4>{} {}</h4><p>{}</p>'.format(_('Terms of Service'), _('Producer'), get_producer_terms()))
html_music_director_terms = format_html('<h4>{} {}</h4><p>{}</p>'.format(_('Terms of Service'), _('Music Director'), get_music_director_terms()))
html_musician_terms = format_html('<h4>{} {}</h4><p>{}</p>'.format(_('Terms of Service'), _('Musician'), get_musician_terms()))